<?php

namespace Uncgits\LoggyWrapper;

use Illuminate\Support\ServiceProvider as Provider;

class ServiceProvider extends Provider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

        $this->loadConfiguration();

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerLoggy();
    }

    /**
     * Load the configuration files and allow them to be published.
     *
     * @author uncgits
     * @return void
     */
    private function loadConfiguration()
    {
        $configPath = __DIR__.'/config.php';

        $this->publishes([$configPath => config_path('loggy.php')], 'config');

        $this->mergeConfigFrom($configPath, 'loggy');
    }


    private function registerLoggy()
    {
        // overrides base app binding
        $this->app->bind('loggy', 'Uncgits\LoggyWrapper\Stream\Writer');
    }
}
