<?php

namespace Uncgits\LoggyWrapper\Stream;

use Monolog\Logger;
use Tolawho\Loggy\Stream\Writer as BaseWriter;
use Uncgits\LoggyWrapper\Events\LoggyMessageLogged;

/**
 * Class Writer
 * @package Uncgits\LoggyWrapper\Stream
 */
class Writer extends BaseWriter
{
    /**
     * Write to log based on the given channel and level
     *
     * @author tolawho
     * @param string $channel
     * @param bool|int $level
     * @param string $message
     * @param array $context
     * @return void
     */
    private function put($channel, $level, $message, array $context = [])
    {
        // from parent, since we can't call parent::put()
        if (!$this->channelExist($channel)) {
            throw new \InvalidArgumentException("The channel named $channel is not exist.");
        }

        if (!isset($this->logger[$channel])) {
            $this->logger[$channel] = new Logger($channel);
            $this->logger[$channel]->pushHandler(
                new Handler(
                    $channel,
                    storage_path(sprintf('logs/%s', $this->channels[$channel]['log'])),
                    $this->channels[$channel]['level']
                )
            );
        }

        $this->logger[$channel]->{$level}($message, $context);

        if (config('loggy.fire_event')) {
            // fire a MessageLogged event
            event(new LoggyMessageLogged($channel, $level, $message, $context));
        }
    }
}
